var mainModule = angular.module('mainModule', []);


mainModule.service('RepositoriesService', function($http){

  this.getRepositoriesList = function(user){
    this.repo = $http.get('https://api.github.com/users/'+user+'/repos');
    return this.repo
  }
});

mainModule.controller('pro', ['$scope','$http','$stateParams','RepositoriesService', function($scope,$http,$stateParams,RepositoriesService){
  $scope.user = $stateParams.project;
  $scope.mas;
  $scope.response = RepositoriesService.getRepositoriesList($scope.user).success(function(data){
    $scope.mas = data;
  });
}]);


