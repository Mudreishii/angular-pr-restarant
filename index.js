var App =  angular.module('App', [
  'ui.router',
  'mainModule'
]);

App.config(function($stateProvider, $urlRouterProvider){
  $urlRouterProvider.otherwise('/project1-perehod');

  $stateProvider
    .state('project1', {
      url: '/project1-perehod?project',
      templateUrl: 'projects/project1.html',
      controller: 'pro'
    })
    .state('project2', {
      url: '/project2-perehod:project',
      templateUrl: 'projects/project2.html',
      controller: 'pro'
    })
    .state('project3', {
      url: '/project3-perehod:project',
      templateUrl: 'projects/project3.html',
      controller: 'pro'
    })
    .state('project4', {
      url: '/project4-perehod:project',
      templateUrl: 'projects/project4.html',
      controller: 'pro'
    })
    .state('project5', {
      url: '/project5-perehod:project',
      templateUrl: 'projects/project5.html',
      controller: 'pro'
    })
});
